<?php

namespace Application\Service;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Application\Entity\Base\IBaseEntity;
/**
 * 
 * Classe Service abstrata que implementa o crud das entidades
 * 
 * @author everton <everton.mailbox@gmail.com>
 *
 */
abstract class AbstractService implements IService {
	
	/**
	 *
	 * @var EntityManager
	 */
	protected $em;
	
	/**
	 *
	 * @var EntityRepository
	 */
	protected $repository;
	
	
	public function __construct($service) {
		$this->em = $service->get ( 'Doctrine\ORM\EntityManager' );
		$this->repository = $this->em->getRepository ( $this->getEntityClassName () );
	}
	
	public function inserir(IBaseEntity $entity){
		$this->validateInstance($entity);
		$this->em->persist($entity);
		$this->em->flush();
	}
	
	public function alterar(IBaseEntity $entity){
		$this->validateInstance($entity);
		$this->em->merge($entity);
		$this->em->flush();
	}
	
	public function excluir(IBaseEntity $entity){
		$this->validateInstance($entity);
		$this->em->refresh($entity);
		$this->em->flush();
	}
	
	/**
	 *
	 * @param int $id
	 */
	public function encontrarPorId($id) {
		$entity = $this->repository->findOneBy(array('id'=>$id));
		return $entity;
	}
	
	public function getTodos() {
		return $this->repository->findAll();
	}
	
	private function validateInstance($entity){
		
		if (!is_subclass_of($entity, 'Application\Entity\Base\IBaseEntity')){
			throw  new InvalidArgumentException('Classe nao originada de IBaseEntity');
		}
		
		
		if ($this->getEntityClassName() != get_class($entity))
		{
			throw  new InvalidArgumentException;
		}
	}
}