<?php

namespace Application\Service;
/**
 * Service destinado a classe TipoLinkPessoal
 * @author marcio <marciobarbosamobile@gmail.com>
 *
 */
class TipoLinkPessoalService extends AbstractService {
	/*
	 * Todas as classes que extendem de service devem conter este construtor
	 * apenas com ele é possivel obter o entity manager
	 */
	public function __construct($service){
		parent::__construct($service);
	}
	/*
	 * Todas as classes que extendem de service devem conter este metodo
	 * nele deve ser retornado o nome inteiro da classe a que se destina este service
	 */
	public function getEntityClassName(){
		return 'Application\Entity\TipoLinkPessoal';
	}
}