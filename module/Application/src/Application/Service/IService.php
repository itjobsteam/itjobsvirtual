<?php

namespace Application\Service;

use Application\Entity\Base\IBaseEntity;
/**
 * 
 * Todos os services do projeto devem herdar desta interface
 * @author everton <everton.mailbox@gmail.com>
 *
 */
interface IService {
	
	/**
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getTodos();
	/**
	 * 
	 * @param integer $id
	 */
	public function encontrarPorId($id);
	/**
	 * @return string
	 */
	public function getEntityClassName();
	/**
	 * 
	 * @param IBaseEntity $entity
	 */
	public function inserir(IBaseEntity $entity);
	/**
	 * 
	 * @param IBaseEntity $entity
	 */
	public function alterar(IBaseEntity $entity);
	/**
	 * 
	 * @param IBaseEntity $entity
	 */
	public function excluir(IBaseEntity $entity);
}