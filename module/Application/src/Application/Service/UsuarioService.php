<?php

namespace Application\Service;


use Application\Entity\Usuario;
/**
 * Service destinado a classe Usuario
 * @author everton <everton.mailbox@gmail.com>
 *
 */
class UsuarioService extends AbstractService {
	/*
	 * Todas as classes que extendem de service devem conter este construtor
	 * apenas com ele é possivel obter o entity manager
	 */
	public function __construct($service){
		parent::__construct($service);
	}
	/*
	 * Todas as classes que extendem de service devem conter este metodo
	 * nele deve ser retornado o nome inteiro da classe a que se destina este service
	 */
	public function getEntityClassName(){
		return 'Application\Entity\Usuario';
	}
	
	/**
	 * 
	 * @param Usuario $usuario
	 * @return boolean
	 */
	public function verificaUsuarioAuticacao(Usuario $usuario){
		$retorno = $this->repository->findOneBy(array('email'=>$usuario->getEmail(), 'senha' => $usuario->getSenha()));
		return $retorno != null;
	}
	
}