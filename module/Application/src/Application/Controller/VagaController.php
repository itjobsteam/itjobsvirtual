<?php


namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Vaga;
use Application\Service\VagaService;

class VagaController extends AbstractActionController {
	
	
	public function indexAction() {
		$service = new VagaService($this->getServiceLocator());
		return new ViewModel (array('model'=> $service->getTodos()));
	}
	
	public function insertAction() {
		if ($this->getRequest()->isPost ()) {
			$vaga = Vaga::fromArray($this->params()->fromPost ());
			$service = new VagaService($this->getServiceLocator());
			$service->inserir($vaga);
			$this->redirect()->toRoute('vagas', array('action'=> 'index'));
		} else {
			return new ViewModel ();
		}
	}
	
	public function deleteAction() {
		return new ViewModel ();
	}
	
	public function editAction() {
		return new ViewModel ();
	}
	
}
