<?php

namespace Application\Controller;

use Application\Entity\Telefone;
use Application\Entity\LinkPessoal;
use Application\Entity\Curso;
use Application\Entity\Contato;
use Application\Entity\Candidato;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\CandidatoService;

class CandidatoController extends AbstractActionController {
	
	public function indexAction() 
	{
		$service = new CandidatoService($this->getServiceLocator());
		return new ViewModel (array('model'=> $service->getTodos()));
	}
	
	public function insertAction() 
	{
		if ($this->getRequest()->isPost()) {
			$candidato = Candidato::fromArray($this->params()->fromPost());
			$service = new CandidatoService($this->getServiceLocator());
			$service->inserir($candidato);
			return $this->redirect ()->toRoute ('candidatos', array('action'=> 'index'));
		}else {
			return new ViewModel ();
		}
	}
	
	public function deleteAction() 
	{
		return new ViewModel ();
	}
	
	public function visualizaAction() {
		return new ViewModel ();
	}
	
	public function editAction() {
		return new ViewModel ();
	}
	
	public function confirmarAction() {
		return new ViewModel ();
	}
}
