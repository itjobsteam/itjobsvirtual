<?php

namespace Application\Controller;

use Zend\View\Helper\ViewModel;
use Application\Service\CompetenciaService;
use Zend\Mvc\Controller\AbstractActionController;

class CompetenciaController extends AbstractActionController {
	public function indexAction() {
		return new ViewModel ();
	}
	public function insertAction() {
		return new ViewModel ();
	}
}