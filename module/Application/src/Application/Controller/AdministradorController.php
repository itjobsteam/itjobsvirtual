<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\AdministradorService;
use Application\Entity\Administrador;

class AdministradorController extends AbstractActionController
{
    public function indexAction()
    {
    	$service = new AdministradorService($this->getServiceLocator());
    	return new ViewModel (array('model'=> $service->getTodos()));

    }

    public function insertAction()
    {
    if ($this->getRequest()->isPost ()) {
			$adm = Administrador::fromArray($this->params()->fromPost ());
			$service = new AdministradorService($this->getServiceLocator());
			$service->inserir($adm);
			$this->redirect()->toRoute('admin', array('action'=> 'index'));
		} else {
			return new ViewModel ();
		}
    	
    }

    public function deleteAction()
    {
      return new ViewModel();
    }

    public function editAction()
    {
      return new ViewModel();
    }

}
