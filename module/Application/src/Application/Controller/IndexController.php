<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Entity\Usuario;
use Zend\Mvc\Controller\Plugin\Redirect;
use Application\Service\UsuarioService;
use Application\Service\CandidatoService;
use Application\Entity\Candidato;

class IndexController extends AbstractActionController {
	public function __construct() {
	}
	public function indexAction() {
		
		
 	/*	$candService = new CandidatoService($this->getServiceLocator());
		
		$usu = new Candidato();
		
		$usu->setCpf('06455017439');
		$usu->setRg('3087991');
		$usu->setEmail('marciobarbosamobile@gmail.com');
		$usu->setNome('Marcio Barbosa Gomes');
		$usu->setSenha('123');
		
		
		$candService->inserir($usu); */
		
		
		// verifico se eh do tipo post
		if ($this->getRequest ()->isPost ()) {
			
			$this->redirect ()->toRoute ( 'dashboard' );
			
			// recupero o usuario que vem do form
			// agora de forma muito mais facil
			/*$user = Usuario::fromArray ( $this->params ()->fromPost () );
			$service = new UsuarioService ( $this->serviceLocator );
			if ($service->verificaUsuarioAuticacao ( $user )) {
				$this->redirect ()->toRoute ( 'dashboard' );
			} else {
				$this->layout ( 'login_layout' );
				$this->redirect ()->toRoute ( 'home' );
			}*/
		} else {
			$this->layout ( 'login_layout' );
		}
		
		return new ViewModel ();
	}
}
