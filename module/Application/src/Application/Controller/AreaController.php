<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Application\Service\AreaCompetenciaService;
use Application\Entity\AreaCompetencia;

class AreaController extends AbstractActionController {
	public function indexAction() {
		$service = new AreaCompetenciaService($this->getServiceLocator());
		return new ViewModel (array('model'=> $service->getTodos()));
	}
	
	public function insertAction() {
	if ($this->getRequest()->isPost ()) {
			$area = AreaCompetencia::fromArray($this->params()->fromPost ());
			$service = new AreaCompetenciaService($this->getServiceLocator());
			$service->inserir($area);
			$this->redirect()->toRoute('areas', array('action'=> 'index'));
		} else {
			return new ViewModel ();
		}
	}
}