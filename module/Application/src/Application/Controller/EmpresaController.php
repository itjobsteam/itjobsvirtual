<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class EmpresaController extends AbstractActionController {
	public function indexAction() {
		return new ViewModel ();
	}
	public function insertAction() {
		return new ViewModel ();
	}
	public function deleteAction() {
		return new ViewModel ();
	}
	public function editAction() {
		return new ViewModel ();
	}
}
