<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class TipoTelefone implements IBaseEntity 
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $tipo;
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	public function setTipo($tipo) {
		$this->tipo = $tipo;
		return $this;
	}
	public function getTipo() {
		return $tipo;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Contato
	 */
	public static function fromArray(array $array) {
		$o = new TipoTelefone();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}