<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 */
class Usuario implements IBaseEntity {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id; // O nome da varidavel de identificação deve ser apenas id e não "idUsuario ou id_usuario", o Doctrine procura por "id"
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $nome;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $cpf;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $rg;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $email;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $senha;
	
	/**
	 * @ORM\Column(type="datetime", name="dataNascimento", nullable = true)
	 */
	protected $data_nascimento;
	
	/**
	 * @ORM\Column(type="integer", nullable = true)
	 */
	protected $sexo;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $foto;
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
	}
	/**
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	public function setNome($nome) {
		$this->nome = $nome;
	}
	public function getNome() {
		return $this->nome;
	}
	public function setCpf($cpf) {
		$this->cpf = $cpf;
	}
	public function getCpf() {
		return $this->cpf;
	}
	public function setRg($rg) {
		$this->rg = $rg;
	}
	public function getRg() {
		return $this->rg;
	}
	public function setEmail($email) {
		$this->email = $email;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setSenha($senha) {
		$this->senha = $senha;
	}
	public function getSenha() {
		return $this->senha;
	}
	public function setData_nascimento($data_nascimento) {
		$this->data_nascimento = $data_nascimento;
	}
	public function getData_nascimento() {
		return $this->data_nascimento;
	}
	public function setSexo($sexo) {
		$this->sexo = $sexo;
	}
	public function getSexo() {
		return $this->sexo;
	}
	public function setFoto($foto) {
		$this->foto = $foto;
	}
	public function getFoto() {
		return $this->foto;
	}
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 * 
	 * @param array $array
	 * @return \Application\Entity\Usuario
	 */
	public static function fromArray(array $array) {
		$o = new Usuario();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}