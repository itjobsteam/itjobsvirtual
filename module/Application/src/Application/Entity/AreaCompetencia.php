<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class AreaCompetencia implements IBaseEntity {
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $nome;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $descricao;
	
	// getters and setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	public function setNome($nome) {
		$this->nome = $nome;
		return $this;
	}
	public function getNome() {
		return $nome;
	}
	public function setDescricao($descricao) {
		$this->descricao = $descricao;
		return $this;
	}
	public function getDescricao() {
		return $descricao;
	}
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array        	
	 * @return \Application\Entity\AreaCompetencia
	 */
	public static function fromArray(array $array) {
		$o = new AreaCompetencia ();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
	
	
}
