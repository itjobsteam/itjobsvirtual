<?php
namespace Application\Entity;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;
/** 
 * @ORM\Entity
*
*/
class Candidato extends Usuario implements IBaseEntity
{
	/** @ORM\ManyToOne(targetEntity="Curso")*/
    protected $curso;

	/** @ORM\ManyToMany(targetEntity="Competencia")*/
	protected $competencias;

	//array de competencias
	public function __construct()
    {
        $this->competencias = new ArrayCollection();
    }

	public function setCurso($curso)
	{
		$this->curso = $curso;
		return $this;
	}
	public function getCurso($curso)
	{
	    return $curso;
	}

	/**
	 * 
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getCompetencias()
	{
		return $this->competencias;
	}
	
	/**
	 * 
	 * @param \Doctrine\Common\Collections\ArrayCollection $competencias
	 */
	public function setCompetencias(ArrayCollection $competencias)
	{
		$this->competencias = $competencias;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Candidato
	 */
	public static function fromArray(array $array) {
		$o = new Candidato();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}

}