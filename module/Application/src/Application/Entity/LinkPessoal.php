<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;
/** @ORM\Entity
*
*/
class LinkPessoal implements IBaseEntity
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

	/** @ORM\Column(type="string", nullable = true) */
    protected $descricao;

	/** @ORM\Column(type="string", nullable = true) */
    protected $href;

	/** @ORM\ManyToOne(targetEntity="TipoLinkPessoal") */
    protected $tipo_link_pessoal;


	// getters/setters
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	public function getId()
	{
	    return $id;
	}

	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
		return $this;
	}
	public function getDecricao()
	{
	    return $descricao;
	}

	public function setHref($href)
	{
		$this->href = $href;
		return $this;
	}
	public function getHref()
	{
	    return $href;
	}

	public function setTipo_link_pessoal($tipo_link_pessoal)
	{
		$this->tipo_link_pessoal = $tipo_link_pessoal;
		return $this;
	}
	public function getTipo_link_pessoal()
	{
	    return $tipo_link_pessoal;
	}
	
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\LinkPessoal
	 */
	public static function fromArray(array $array) {
		$o = new LinkPessoal();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}