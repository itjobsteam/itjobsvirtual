<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Curso implements IBaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $descricao;
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	public function setDescricao($descricao) {
		$this->id_descricao = $descricao;
		return $this;
	}
	public function getDesricao() {
		return $descricao;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Curso
	 */
	public static function fromArray(array $array) {
		$o = new Curso();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}