<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Telefone implements IBaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="integer", nullable = true)
	 */
	protected $ddd;
	/**
	 * @ORM\Column(type="integer", nullable = true)
	 */
	protected $numero;
	/**
	 * @ORM\Column(type="integer", nullable = true)
	 */
	protected $tipo_telefone;
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	public function setDdd($ddd) {
		$this->ddd = $ddd;
		return $this;
	}
	public function getDdd() {
		return $ddd;
	}
	public function setNumero($numero) {
		$this->numero = $numero;
		return $this;
	}
	public function getNumero() {
		return $numero;
	}
	public function setTipo_telefone($Tipo_telefone) {
		$this->tipo_telefone = $tipo_telefone;
		return $this;
	}
	public function getTipo_Telefone() {
		return $tipo_telefone;
	}
	
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Telefone
	 */
	public static function fromArray(array $array) {
		$o = new Telefone();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}
