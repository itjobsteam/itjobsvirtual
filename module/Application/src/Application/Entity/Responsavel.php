<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Responsavel extends Usuario implements IBaseEntity
{
	
	/**
	 * @ORM\ManyToMany(targetEntity="Empresa")
	 */
	protected $empresa;
	
	// array de empresas
	public function __construct() {
		$this->empresa = new ArrayCollection ();
	}
	
	public function setEmpresa($empresa) {
		$this->empresa = $empresa;
		return $this;
	}
	public function getEmpresa($empresa) {
		return $empresa;
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Responsavel
	 */
	public static function fromArray(array $array) {
		$o = new Responsavel();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}	