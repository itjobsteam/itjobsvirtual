<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;
/**
 * 
 * @ORM\Entity
 * 
 */
class Competencia implements IBaseEntity
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

	/** @ORM\Column(type="string", nullable = true)*/
	protected $nome;

	/** @ORM\Column(type="string", nullable = true)*/
	protected $descricao;

	/** @ORM\ManyToOne(targetEntity="AreaCompetencia") */
	protected $area_competencia;


	//getters and setters
	public function setId($id)
	{
		$this->id = $id;
		
	}
	public function getId()
	{
	    return $id;
	}

	public function setNome($nome)
	{
		$this->nome = $nome;
		
	}
	public function getNome()
	{
	    return $nome;
	}

	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
		
	}
	public function getDescricao()
	{
	    return $descricao;
	}

	public function setArea_competencia($area_competencia)
	{
		$this->area_competencia = $area_competencia;
		
	}
	public function getArea_competencia($area_competencia)
	{
	    return $area_competencia;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Competencia
	 */
	public static function fromArray(array $array) {
		$o = new Competencia();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}



}
