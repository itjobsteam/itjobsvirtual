<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Empresa implements IBaseEntity{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $cnpj;
	
	/**
	 * @ORM\Column(type="string", name="nomeFantasia", nullable = true)
	 */
	protected $nome_fantasia;
	
	/**
	 * @ORM\Column(type="string", name="razaoSocial", nullable = true)
	 */
	protected $razao_social;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $foto;
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $site;
	/**
	 * @ORM\ManyToOne(targetEntity="Vaga")
	 */
	protected $vagas;
	/**
	 * @ORM\ManyToMany(targetEntity="Responsavel")
	 */
	protected $responsaveis;
	
	// array de empresas
	public function __construct() {
		$this->vaga = new ArrayCollection ();
		$this->responsaveis = new ArrayCollection ();
	}
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	public function setCnpj($cnpj) {
		$this->cnpj = $cnpj;
		return $this;
	}
	public function getCnpj() {
		return $cnpj;
	}
	public function setNome_fantasia($nome_fantasia) {
		$this->nome_fantasia = $nome_fantasia;
		return $this;
	}
	public function getNome_fantasia() {
		return $nome_fantasia;
	}
	public function setRazao_social($razao_social) {
		$this->razao_social = $razao_social;
		return $this;
	}
	public function getRazao_social() {
		return $razao_social;
	}
	public function setFoto($foto) {
		$this->foto = $foto;
		return $this;
	}
	public function getFoto() {
		return $foto;
	}
	public function setSite($site) {
		$this->site = $site;
		return $this;
	}
	public function getSite() {
		return $site;
	}
	public function getVagas() {
		return $this->vagas;
	}
	public function setVagas($vagas) {
		$this->vagas = $vagas;
	}
	public function getResponsaveis() {
		return $this->responsaveis;
	}
	public function setResponsaveis($responsaveis) {
		$this->responsaveis = $responsaveis;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Empresa
	 */
	public static function fromArray(array $array) {
		$o = new Empresa();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}
