<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Contato implements IBaseEntity
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Telefone")
	 */
	protected $telefones;
	
	/**
	 * @ORM\ManyToMany(targetEntity="LinkPessoal")
	 */
	protected $link_pessoal;
	
	// construtor carregar os arrays
	public function __construct() {
		$this->telefones = new ArrayCollection ();
		$this->link_pessoal = new ArrayCollection ();
	}
	
	// getters/setters
	public function setId($id) {
		$this->id = $id;
		return $this;
	}
	public function getId() {
		return $id;
	}
	
	
	/**
	 *
	 * @return Ambigous <\Doctrine\Common\Collections\ArrayCollection, Telefone>
	 */
	public function getTelefones() {
		return $this->telefones;
	}
	
	/**
	 *
	 * @param
	 *        	Ambigous <\Doctrine\Common\Collections\ArrayCollection, Telefone> $telefones
	 */
	public function setTelefones() {
		$this->telefones = $telefones;
	}

	/**
	 *
	 * @return \Doctrine\Common\Collections\ArrayCollection
	 */
	public function getLinkisPessoais() {
		return $this->link_pessoal;
	}
	
	/**
	 *
	 * @param unknown $links_pessais        	
	 */
	public function setLinkisPessoais($links_pessais) {
		$this->link_pessoal = $links_pessais;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Contato
	 */
	public static function fromArray(array $array) {
		$o = new Contato();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
}