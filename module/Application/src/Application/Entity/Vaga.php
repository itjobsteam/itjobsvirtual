<?php

namespace Application\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;

/**
 * @ORM\Entity
 */
class Vaga implements IBaseEntity{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 * @ORM\Column(type="integer")
	 */
	protected $id;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $descricao;
	
	/**
	 * @ORM\Column(type="integer", nullable = true)
	 */
	protected $quantidade;
	
	/**
	 * @ORM\Column(type="decimal", nullable = true)
	 */
	protected $salario;
	
	/**
	 * @ORM\Column(type="datetime", nullable = true)
	 */
	protected $validade;
	
	/**
	 * @ORM\Column(type="boolean", name="exibeEmpresa", nullable = false)
	 */
	protected $exibe_empresa;
	
	/**
	 * @ORM\Column(type="string", nullable = true)
	 */
	protected $turno;
	
	/**
	 * @ORM\ManyToMany(targetEntity="Competencia")
	 */
	protected $competencia;
	
	/**
	 * @ORM\ManyToOne(targetEntity="Empresa")
	 */
	protected  $empresa;
	
	public function __construct() {
		$this->competencia = new ArrayCollection ();
	}
	
	public function setId($id) {
		$this->id = $id;
	}
	public function getId() {
		return $this->id;
	}
	
	public function setDescricao($descricao) {
		$this->descricao = $descricao;
	}
	public function getDescricao() {
		return $this->descricao;
	}
	public function setQuantidade($quantidade) {
		$this->quantidade = $quantidade;
	}
	public function getQuantidade() {
		return $this->quantidade;
	}
	
	public function setSalario($salario) {
		$this->salario = $salario;
	}
	public function getSalario() {
		return $this->salario;
	}
	public function setValidade($validade) {
		$this->validade = $validade;
	}
	public function getValidade() {
		return $this->validade;
	}
	public function setExibe_empresa($exibe_empresa) {
		$this->exibe_empresa = $exibe_empresa;
	}
	/**
	 * @return boolean
	 */
	public function getExibe_empresa() {
		return $this->exibe_empresa;
	}
	public function setTurno($turno) {
		$this->turno = $turno;
	}
	public function getTurno() {
		return $this->turno;
	}
	public function setCompetencia(Competencia $competencia) {
		$this->competencia = $competencia;
	}
	public function getCompetencia() {
		return $this->competencia;
	}
	public function setEmpresa(Empresa $empresa){
		$this->empresa = $empresa;
	}
	/**
	 * @return Empresa
	 */
	public function getEmpresa(){
		return $this->empresa;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Vaga
	 */
	public static function fromArray(array $array) {
		$o = new Vaga();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
	
}
