<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;


/** @ORM\Entity 
*/
class Administrador extends Usuario implements IBaseEntity
{
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Administrador
	 */
	public static function fromArray(array $array) {
		$o = new Administrador();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
}
