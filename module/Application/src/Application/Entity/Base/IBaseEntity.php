<?php

namespace Application\Entity\Base;
/**
 * 
 * Todas as entidades do projeto devem herdar desta interface
 * 
 * @author everton <everton.mailbox@gmail.com>
 *
 */
interface IBaseEntity {
	
	/**
	 *
	 * @return int
	 */
	public function getId();
	
	/**
	 *
	 * @return array
	 */
	public function toArray();
	/*
	 * implementacao do toArray
	 * 
	 * return get_object_vars ( $this );
	 *
	 */
	
	/**
	 *
	 * @param array $array        	
	 */
	public static function fromArray(array $array);
	/*
	 * exemplo implementacao do fromArray
	 * 
	 * public static function fromArray(array $array) {
	 * $o = new Usuario();
	 * foreach ( $array as $key => $value ) {
	 * 			$o->$key = $value;
	 * 		}
	 * return $o;
	 * }
	 * 
	 * 
	 */
}