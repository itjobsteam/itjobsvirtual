<?php
namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;
use Application\Entity\Base\IBaseEntity;
/**
 * 
 *  @ORM\Entity 
*
*/
class TipoLinkPessoal implements IBaseEntity
{
    /**
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    * @ORM\Column(type="integer")
    */
    protected $id;

	/** @ORM\Column(type="string", nullable = true) */
    protected $descricao;

	/** @ORM\Column(type="string", nullable = true) */
    protected $href;

	/** @ORM\Column(type="string", nullable = true) */
    protected $icone;


	// getters/setters
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	public function getId()
	{
	    return $id;
	}

	public function setDescricao($descricao)
	{
		$this->descricao = $descricao;
		return $this;
	}
	public function getDecricao()
	{
	    return $descricao;
	}

	public function setHref($href)
	{
		$this->href = $href;
		return $this;
	}
	public function getHref()
	{
	    return $href;
	}
	
	public function setIcone($icone)
	{
		$this->icone = $icone;
		return $this;
	}
	public function getIcone()
	{
	    return $icone;
	}
	
	public function toArray() {
		return get_object_vars ( $this );
	}
	
	
	/**
	 *
	 * @param array $array
	 * @return \Application\Entity\Contato
	 */
	public static function fromArray(array $array) {
		$o = new TipoLinkPessoal();
		foreach ( $array as $key => $value ) {
			$o->$key = $value;
		}
		return $o;
	}

}