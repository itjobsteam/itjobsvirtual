<?php
return array (
		'router' => array (
				'routes' => array (
						'home' => array (
								'type' => 'Zend\Mvc\Router\Http\Literal',
								'options' => array (
										'route' => '/',
										'defaults' => array (
												'controller' => 'Application\Controller\Index',
												'action' => 'index' 
										)
								) 
						),
						'dashboard' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/dashboard[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Dashboard',
												'action' => 'index'
										)
								)
						),
						'candidatos' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/candidatos[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Candidato',
												'action' => 'index' 
										) 
								) 
						),
						'empresas' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/empresas[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Empresa',
												'action' => 'index' 
										) 
								) 
						),
						'vagas' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/vagas[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Vaga',
												'action' => 'index' 
										) 
								) 
						),
						'competencias' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/competencias[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Competencia',
												'action' => 'index' 
										) 
								) 
						),
						'admin' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/admin[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Administrador',
												'action' => 'index'
										)
								)
						),
						'areas' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/areas[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Area',
												'action' => 'index'
										)
								)
						),
						'cursos' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/cursos[/:action][/:id]',
										'constraints' => array (
												'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
										),
										'defaults' => array (
												'controller' => 'Application\Controller\Curso',
												'action' => 'index'
										)
								)
						),
						
						// The following is a route to simplify getting started creating
						// new controllers and actions without needing to create a new
						// module. Simply drop new controllers in, and you can access them
						// using the path /application/:controller/:action
						'application' => array (
								'type' => 'Literal',
								'options' => array (
										'route' => '/application',
										'defaults' => array (
												'__NAMESPACE__' => 'Application\Controller',
												'controller' => 'Index',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'default' => array (
												'type' => 'Segment',
												'options' => array (
														'route' => '/[:controller[/:action]]',
														'constraints' => array (
																'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
														),
														'defaults' => array () 
												) 
										) 
								) 
						) 
				) 
		),
		'service_manager' => array (
				'abstract_factories' => array (
						'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
						'Zend\Log\LoggerAbstractServiceFactory' 
				),
				'aliases' => array (
						'translator' => 'MvcTranslator' 
				) 
		),
		'translator' => array (
				'locale' => 'en_US',
				'translation_file_patterns' => array (
						array (
								'type' => 'gettext',
								'base_dir' => __DIR__ . '/../language',
								'pattern' => '%s.mo' 
						) 
				) 
		),
		'controllers' => array (
				'invokables' => array (
						'Application\Controller\Index' => 'Application\Controller\IndexController',
						'Application\Controller\Dashboard' => 'Application\Controller\DashboardController',
						'Application\Controller\Area' => 'Application\Controller\AreaController',
						'Application\Controller\Candidato' => 'Application\Controller\CandidatoController',
						'Application\Controller\Empresa' => 'Application\Controller\EmpresaController',
						'Application\Controller\Administrador' => 'Application\Controller\AdministradorController',
						'Application\Controller\Vaga' => 'Application\Controller\VagaController',
						'Application\Controller\Competencia' => 'Application\Controller\CompetenciaController',
						'Application\Controller\Curso' => 'Application\Controller\CursoController'
				)
		),
		'view_manager' => array (
				'display_not_found_reason' => true,
				'display_exceptions' => true,
				'doctype' => 'HTML5',
				'not_found_template' => 'error/404',
				'exception_template' => 'error/index',
				'template_map' => array (
						'layout_principal' => __DIR__ . '/../view/layout/layout.phtml',
						'login_layout' => __DIR__ . '/../view/layout/login_layout.phtml',
						'empty_layout' => __DIR__ . '/../view/layout/empty.phtml',
						'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
						'error/404' => __DIR__ . '/../view/error/404.phtml',
						'error/index' => __DIR__ . '/../view/error/index.phtml' 
				),
				'template_path_stack' => array (
						__DIR__ . '/../view' 
				) 
		),
		
		// Placeholder for console routes
		'console' => array (
				'router' => array (
						'routes' => array () 
				) 
		),
		
		// Configuração do Doctrine
		
		'doctrine' => array (
				'driver' => array (
						'application_entities' => array (
								'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
								'cache' => 'array',
								'paths' => array (
										__DIR__ . '/../src/Application/Entity' 
								) 
						),
						
						'orm_default' => array (
								'drivers' => array (
										'Application\Entity' => 'application_entities' 
								) 
						) 
				) 
		) 
)
;
